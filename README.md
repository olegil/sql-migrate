SQL-Migrate How to safely migrate your database changes:

SQL-Migrate is simple script that can be a useful tool 
for you to migrate your database changes and apply to server
with less to no hassle. Usually a large scale project needs
constant modifications to how the database design is structured.
When your team is finding it difficult to add/execute queries
by themselves in their machines, you can simply use this script.


Steps:

1. Load/upload this up to your website's root folder.
2. Change the config file's path in the migrate.php file.
3. Add/change your queries.
4. For Linux/Unix system users, the self-executable script
requires a binary path on top of file. Change it accordingly.
It must be usually /usr/local/bin/php or /usr/bin/php
5. Add it to your project's repository.
6. Try improving the script :-)


OpenSource promise:

SQL-Migrate, like most of VizCreations projects is GPL licensed
and completely opensource, so it requires the contributors to 
be the ones responsible to make it a better product.


Reach/Contact:

Contact our development team at www.vizcreations.com or drop an email
to viz@vizcreations.com or viju.kantah@gmail.com

Or simply let us know what you think about SQL-Migrate at 
https://www.facebook.com/pages/VizCreations/196503337046748
